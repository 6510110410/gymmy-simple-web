from flask_wtf import FlaskForm
from wtforms import StringField
from wtforms import validators
from wtforms import widgets


class FolioForm(FlaskForm): 
    name = StringField("Name", validators=[validators.DataRequired()])
    email = StringField(
        "Email", validators=[validators.DataRequired(), validators.Length(min=3)]
    )
    address = StringField("Address", widget=widgets.TextArea())
    biography = StringField("Biography", widget=widgets.TextArea())
    family = StringField("Family", widget=widgets.TextArea())
    education = StringField("Education", widget=widgets.TextArea())
    
    